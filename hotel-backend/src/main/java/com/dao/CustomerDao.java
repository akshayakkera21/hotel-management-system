package com.dao;

import java.util.List;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.model.CustomerRegis;

@Service
public class CustomerDao {
    @Autowired // dependency injection
    private CustomerRepo custRepo;

    public void register(CustomerRegis cust) {
        custRepo.save(cust);
    }

    public CustomerRegis findByEmail(String email) {
        return custRepo.findByEmail(email);
    }

    public List<CustomerRegis> getAllCustomers() {
        return custRepo.findAll();
    }

    public void deleteByName(String name) {
        Optional<CustomerRegis> customerOptional = custRepo.findByName(name);
        if (customerOptional.isPresent()) {
            custRepo.delete(customerOptional.get());
        } else {
            throw new EntityNotFoundException("Entity with name " + name + " not found");
        }
    }

    public CustomerRegis updateByName(String name, CustomerRegis updatedCustomer) {
        Optional<CustomerRegis> optionalCustomer = custRepo.findByName(name);
        return custRepo.save(updatedCustomer);
    }

    public CustomerRegis login(String email, String password) {
        return custRepo.findByEmailAndPassword(email, password);
    }
}
