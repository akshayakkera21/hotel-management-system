package com.ts.hotel_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.dao", "com.ts.hotel_backend"})
@EnableJpaRepositories(basePackages = "com.dao")
@EntityScan(basePackages = "com.model") 
public class HotelBackendApplication {
    public static void main(String[] args) {
        SpringApplication.run(HotelBackendApplication.class, args);
    }
}
