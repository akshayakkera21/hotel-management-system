import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  private checkInDate!: Date;
  private numberOfGuests!: number;
  private checkOutDate!: Date;

  constructor() { }

  setBookingDetails(checkInDate: Date, numberOfGuests: number, checkOutDate: Date) {
    this.checkInDate = checkInDate;
    this.numberOfGuests = numberOfGuests;
    this.checkOutDate = checkOutDate;
  }

  getCheckInDate(): Date {
    return this.checkInDate;
  }

  getNumberOfGuests(): number {
    return this.numberOfGuests;
  }

  getCheckOutDate(): Date {
    return this.checkOutDate;
  }
}
