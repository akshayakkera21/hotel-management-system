import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

interface LoginResponse {
  success: boolean;
  
}
@Injectable({
  providedIn: 'root'
})
export class HotelService {
  private apiUrl = 'http://localhost:8080/register';

  constructor(private http: HttpClient) {}

  registerUser(user: any): Observable<any> {
    return this.http.post<any>(this.apiUrl,user);
  }
  custLogin(credentials: any): Observable<LoginResponse> {
      return this.http.get<LoginResponse>(`http://localhost:8080/show/${credentials.email}/${credentials.password}`);
  }
  private hotels: any[] = [
    {
      id: "1",
      img: "/assets/Images/hotel1.avif",
      name: "ARU Blu Plaza Hotel,Banjara Hills,Hyderabad",
      loc: "8-2-409 Road No. 6, Banjara Hills3.93 miles/6.33 kilometers from Hyderabad",
      price: "8500.00",
      dispri: "7500.00",
      sub: "Air conditioning"
    },
    {
      id: "2",
      img: "/assets/Images/hotel2.avif",
      name: "ARU Hotel Hyderabad Hitec City",
      loc: "Gachibowli, Miyapur Road9.22 miles/14.84 kilometers from Hyderabad",
      price: "9400.00",
      dispri: "8400.00",
      sub: "Air conditioning , Swimming Pool"
    },
    {
      id: "3",
      img: "/assets/Images/hotel3.avif",
      name: "ARU Hotel Hyderabad",
      loc: "Gachibowli, Miyapur Road9.22 miles/14.84 kilometers from Hyderabad",
      price: "9600.00",
      dispri: "8600.00",
      sub: "Air conditioning , Restaurant"
    }
  ];

  getHotelById(id:number) {
    return this.hotels.find(hotel => hotel.id === id);
  }
}
