import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormDataService {

  private formData = new BehaviorSubject<any>({});
  currentFormData = this.formData.asObservable();

  constructor() { }

  sendFormData(data: any) {
    this.formData.next(data);
  }
}
