import { Component,OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { HotelService } from '../hotel.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {
  credentials = { email: '', password: '' };

  constructor(private router: Router, private service: HotelService,private toastr: ToastrService) {}

  ngOnInit(): void {}

  async loginSubmit() {
    try {
      const data = await this.service.custLogin(this.credentials).toPromise();
      console.log('Full Service Response:', data);
      if(this.credentials.email == "admin@gmail.com" && this.credentials.password == "admin123"){
        this.showAdmin();
        this.router.navigate(['/admin']);
      }
      else if (data) {
        this.showSuccess();
        this.router.navigate(['/nav']);
      } else {
        this.showError();
      }
    } catch (error) {
      console.error('Service Error:', error);
      this.showError();
    }
  }
  showSuccess() {
    this.toastr.success('Yah! Login is Success');
  }
  showError() {
    this.toastr.error('Your Login is UnSuccess');
  }
  signup(){
    this.router.navigate(['/signup']);
  }
  showAdmin(){
    this.toastr.error('Welcome Admin!');
  }
}
