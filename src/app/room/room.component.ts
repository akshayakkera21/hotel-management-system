import { Component } from '@angular/core';
import { BookingService } from '../booking.service';
import { ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HotelService } from '../hotel.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent {
  checkInDate: Date;
  numberOfGuests: number;
  checkOutDate!:Date;
  hotel: any;
  rooms: {
    img: string;
    name: string;
    disprice: string;
    price: string;
    bed: string;
    size: string;
  }[] = [
    { img: "/assets/Images/room1.avif", name: "Deluxe Room", price: "8500.00", disprice: "8500.00", bed: "bed type :2 twin or 1 king", size: "473 sq.ft" },
     { img: "/assets/Images/room2.avif", name: "Junior Suite", price: "9500.00", disprice: "11106.00", bed: "bed type :2 twin or 1 king", size: "624 sq.ft" },
    { img: "/assets/Images/room2.avif", name: "Suite", price: "10500.00", disprice: "15156.00", bed: "bed type :2 twin or 1 king", size: "695 sq.ft" },
    { img: "/assets/Images/room3.avif", name: "One Bedroom Suite", price: "11500.00", disprice: "18756.00", bed: "bed type:2 twin or 1 king", size: "1,291 sq.ft" },
    { img: "/assets/Images/room4.avif", name: "Presidential Suite", price: "12500.00", disprice: "31500.00", bed: "bed type :2 twin or 1 king", size: "1,786 sq.ft" }
   ];

  constructor(private bookingService: BookingService ,private cdr: ChangeDetectorRef,private route: ActivatedRoute ,private hotelService: HotelService) {
    this.checkInDate = this.bookingService.getCheckInDate();
    this.numberOfGuests = this.bookingService.getNumberOfGuests();
    this.calculateRoomPrices();
  }
  // ngOnInit(): void {
  //   const hotelId = this.route.snapshot.params['id']; // Assuming the hotel ID is passed as a route parameter
  //   this.hotel = this.hotelService.getHotelById(+hotelId);

  // }
  calculateRoomPrices() {
    this.rooms.forEach(room => {
      const basePrice = +room.price; // Convert price to a number
      let actualPrice: number;
      let discountedPrice: number;
  
      // Calculate actual price based on the number of guests
      if (this.numberOfGuests >= 2 && this.numberOfGuests <= 8) {
        // Actual price increases with the number of guests
        actualPrice = basePrice + (this.numberOfGuests - 2) * 500; // Increase by 500 for each additional guest starting from the 3rd guest
      } else {
        // For less than 2 guests or more than 8 guests, use base price
        actualPrice = basePrice;
      }
  
      // Discounted price is 10% less than the actual price
      discountedPrice = actualPrice * 0.9;
  
      // Assign calculated prices back to the room object
      room.price = actualPrice.toFixed(2);
      room.disprice = discountedPrice.toFixed(2);
    });
  }
  ngOnInit() {
    this.checkInDate = this.bookingService.getCheckInDate();
    this.numberOfGuests = this.bookingService.getNumberOfGuests();
    this.checkOutDate = this.bookingService.getCheckOutDate();
  }
}
