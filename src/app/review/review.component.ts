import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Review } from '../Review';
import { ReviewService } from '../review.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  feedbackForm: FormGroup;
  feedbacks: Review[] = [];
  submitted = false;

  constructor(private formBuilder: FormBuilder, private feedbackService: ReviewService) { 
    this.feedbackForm = this.formBuilder.group({
      ['rating']: ['', Validators.required],
      ['name']: ['', Validators.required],
      ['email']: ['', [Validators.required, Validators.email]],
      ['review']: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    // get the feedbacks from the server
    this.feedbackService.getAllFeedbacks().subscribe(data => {
      this.feedbacks = data;
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.feedbackForm.controls; }

  // submit the form data to the server
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.feedbackForm.invalid) {
      return;
    }

    // create a new feedback object from the form values
    const feedback = new Review(
      0,
      this.f['rating'].value,
      this.f['name'].value,
      this.f['email'].value,
      this.f['review'].value
    );

    // post the feedback to the server and update the feedbacks array
    this.feedbackService.addFeedback(feedback).subscribe(data => {
      this.feedbacks.push(data);
    });

    // reset the form
    this.feedbackForm.reset();
    this.submitted = false;
  }
}