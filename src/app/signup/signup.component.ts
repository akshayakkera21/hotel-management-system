import { Component, OnInit } from '@angular/core';
import { HotelService } from '../hotel.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [HotelService]
})
export class SignupComponent {

  user: any = {
    email: '',
    name: '',
    password: '',
    gender: ''
  };

  constructor(private userService: HotelService, private router: Router,private service: HotelService,private toastr: ToastrService) { }

  onSubmit() {

    console.log('Registration Data:', this.user);

    this.userService.registerUser(this.user).subscribe(
      (response) => {
        this.router.navigate(['/login']);
        this.showSuccess();
        console.log('User registered successfully:', response);
      },
      (error) => {
        console.error('Failed to register user:', error);
      }
    );
  }
  showSuccess() {
    this.toastr.success('Yah! Registration is Success');
  }
  showError() {
    this.toastr.error('Your Registration is UnSuccess');
  }
}
