import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Review } from './Review';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  private baseUrl = 'http://localhost:8080'; 

  constructor(private http: HttpClient) { }

  
  getAllFeedbacks(): Observable<Review[]> {
    return this.http.get<Review[]>(`${this.baseUrl}/getReviews`);
  }

  
  addFeedback(feedback: Review): Observable<Review> {
    return this.http.post<Review>(`${this.baseUrl}/posted`, feedback);
  }
}