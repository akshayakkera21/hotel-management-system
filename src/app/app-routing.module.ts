import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RoomComponent } from './room/room.component';
import { CardComponent } from './card/card.component';
import { LogoutComponent } from './logout/logout.component';
import { NavComponent } from './nav/nav.component';
import { AdminComponent } from './admin/admin.component';
import { RazopayComponent } from './razopay/razopay.component';
import { SucessComponent } from './sucess/sucess.component';
import { ReviewComponent } from './review/review.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path:"home", component : HomeComponent},
  {path:"signup",component : SignupComponent},
  {path:"login" ,component : LoginComponent},
  {path:"room",component:RoomComponent},
  {path:"card",component:CardComponent},
  {path:"logout",component:LogoutComponent},
  {path:"nav",component:NavComponent},
  {path:"admin",component:AdminComponent},
  {path:"pay",component:RazopayComponent},
  {path:"suc",component:SucessComponent},
  {path:"rev",component:ReviewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
