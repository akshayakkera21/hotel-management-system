import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Details } from './Details';

@Injectable({
  providedIn: 'root'
})
export class BookingDetailsService {
  private baseUrl = 'http://localhost:8080'; 

  constructor(private http: HttpClient) { }
  
  getAllBookings(): Observable<Details[]> {
    return this.http.get<Details[]>(`${this.baseUrl}/getDetails`);
  }

  addBookingDetails(details: Details): Observable<Details> {
    return this.http.post<Details>(`${this.baseUrl}/posted`, details);
  }
}
