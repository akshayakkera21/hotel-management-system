import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RazopayComponent } from './razopay.component';

describe('RazopayComponent', () => {
  let component: RazopayComponent;
  let fixture: ComponentFixture<RazopayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RazopayComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RazopayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
