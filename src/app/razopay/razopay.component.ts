import {  Component } from '@angular/core';
import { BookingService } from '../booking.service';
import { FormDataService } from '../form-data.service';
import { BookingDetailsService } from '../booking-details.service';
import { Details } from '../Details';
import { Router } from '@angular/router';
import { OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
declare var Rozorpay:any;

@Component({
  selector: 'app-razopay',
  templateUrl: './razopay.component.html',
  styleUrl: './razopay.component.css'
})
export class RazopayComponent implements OnInit{
  DetailsForm!: FormGroup;
  showForm: boolean = true;
  title: string = '';
  firstName: string = '';
  lastName: string = '';
  email: string = '';
  receiveNews: boolean = false;
  address: string = '';
  phoneNumber: string = '';
  payNow() {
    const RozorpayOptions={
      description:'sample Razorpay Demo',
      currency: 'INR',
      amount :30000,
      name : 'AKSHAY',
      key :'rzp_test_Pa80oz6OTakhkW	',
      image:'',


      prefil:{
        name :'akshay',
        email : 'akshayakkera21@gmail.com',
        phone : "9100292609",
      },
      theme:{
        color:'#f37254',
      },
      modal :{
        ondismiss : ()=>{
          console.log('dismissed')
        }
      }
    }
    const sucessCallback=(paymentid:any)=>{
      console.log(paymentid);
    }

    const failureCallback=(e:any)=>{
      console.log(e);
    }
    Rozorpay.open(RozorpayOptions,sucessCallback,failureCallback);

  }
  checkInDate!: Date;
  numberOfGuests!: number;
  checkOutDate!:Date;

  constructor(private bookingService: BookingService,private formDataService: FormDataService,private bookingDetailsService: BookingDetailsService,private router: Router,private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createForm();
    this.checkInDate = this.bookingService.getCheckInDate();
    this.numberOfGuests = this.bookingService.getNumberOfGuests();
    this.checkOutDate=this.bookingService.getCheckOutDate();
  }
  submitForm(): void {
    // Handle form submission (e.g., payment processing, saving form data, etc.)
    this.saveFormData();
    this.navigateToOrderReceipt();
  }
  private saveFormData(): void {
    // Save form data to your service or perform any other action
    const details: Details = {
      id: 0,
      firstname: this.firstName,
      lastname: this.lastName,
      email: this.email,
      address: this.address,
      phonenumber: this.phoneNumber
    };
    this.bookingDetailsService.addBookingDetails(details).subscribe(
      () => console.log('Booking details submitted successfully.'),
      error => console.error('Error submitting booking details:', error)
    );
  }

  private navigateToOrderReceipt(): void {
    // Navigate to the order receipt component with form details
    this.router.navigate(['/order-receipt'], { state: { formData: { ...this } } });
  }
  createForm() {
    // Create FormGroup and initialize form controls
    this.DetailsForm = this.formBuilder.group({
      title: [''],
      firstName: [''],
      lastName: [''],
      email: [''],
      receiveNews: [false],
      address: [''],
      phoneNumber: ['']
    });
  }
}
// // bill.component.ts
// import { Component, OnInit } from '@angular/core';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { Router } from '@angular/router';
// import { HttpClient } from '@angular/common/http';
// import { OrderService } from '../orderservice.service';

// @Component({
//   selector: 'app-bill',
//   templateUrl: './bill.component.html',
//   styleUrls: ['./bill.component.css'],
// })
// export class BillComponent implements OnInit {
//   orderForm!: FormGroup;
//   apiUrl = 'http://localhost:8080'; // Update with your actual backend API URL

//   constructor(
//     private fb: FormBuilder,
//     private router: Router,
//     private orderService: OrderService,
//     private http: HttpClient
//   ) {}

//   ngOnInit(): void {
//     this.orderForm = this.fb.group({
//       name: ['', Validators.required],
//       email: ['', [Validators.required, Validators.email]],
//       address: ['', Validators.required],
//       phone: ['', Validators.required],
//       paymentMethod: ['', Validators.required],
//     });
//   }

//   placeOrder(): void {
//     if (this.orderForm.valid) {
//       const orderDetails = this.orderForm.value;

//       // Update order details using the OrderService
//       this.orderService.updateOrderDetails(orderDetails);

//       // Make the HTTP request directly
//       this.http.post(`${this.apiUrl}/orders/createOrder`, orderDetails).subscribe(
//         (response) => {
//           console.log('Order placed successfully:', response);
//           alert('Order placed successfully!'); // Display browser alert
//           this.router.navigate(['/orderreceipt']);
//         },
//         (error) => {
//           console.error('Failed to place order:', error);
//           alert('Failed to place order. Please try again.'); // Display browser alert
//         }
//       );
//     } else {
//       alert('Please fill in all required fields.');
//     }
//   }
// }