export class Review {
    id: number;
    rating: number;
    name: string;
    email: string;
    review: string;
  
    constructor(id: number, rating: number, name: string, email: string, review: string) {
      this.id = id;
      this.rating = rating;
      this.name = name;
      this.email = email;
      this.review = review;
    }
  }