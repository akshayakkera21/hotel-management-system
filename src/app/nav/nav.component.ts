import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BookingService } from '../booking.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {
  bookingForm: FormGroup;

  constructor(
    private router: Router,
    private bookingService: BookingService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder
  ) {
    this.bookingForm = this.formBuilder.group({
      checkInDate: ['', Validators.required],
      checkOutDate: ['', Validators.required],
      numberOfGuests: ['', Validators.required]
    }, { validator: this.checkInDateValidator });
  }

  goToRoomPage() {
    if (this.bookingForm.invalid) {
      this.toastr.error('Please fill in all required fields.');
      return;
    }

    const { checkInDate, checkOutDate, numberOfGuests } = this.bookingForm.value;
    this.bookingService.setBookingDetails(checkInDate, numberOfGuests, checkOutDate);
    this.router.navigate(['/card']);
  }

  checkInDateValidator(group: FormGroup) {
    const checkInDate = group.controls['checkInDate'].value;
    const today = new Date();
    today.setHours(0, 0, 0, 0);

    if (checkInDate < today) {
      return { pastDate: true };
    }

    return null;
  }
}
