import { Component } from '@angular/core';
import { BookingService } from '../booking.service';
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrl: './card.component.css'
})
export class CardComponent {
    card:any;
    checkInDate!: Date;
    checkOutDate!: Date;
    numberOfGuests!: number;
    constructor(private bookingService: BookingService){
      this.card=[{"id":"1", "img" : "/assets/Images/hotel1.avif","name":"ARU Blu Plaza Hotel,Banjara Hills,Hyderabad","loc":"8-2-409 Road No. 6, Banjara Hills3.93 miles/6.33 kilometers from Hyderabad",
                   "price" : "8500.00" ,"dispri" :"7500.00" ,"sub":"Air conditioning"},
                 {"id":"2","img" : "/assets/Images/hotel2.avif","name":"ARU Hotel Hyderabad Gachibowli","loc":"Gachibowli, Miyapur Road9.22 miles/14.84 kilometers from Hyderabad",
                   "price":"9400.00","dispri":"8400.00","sub":"Air conditioning , Swimming Pool"},
                   {"id":"3","img" : "/assets/Images/hotel3.avif","name":"ARU Hotel Mindspace Hyderabad ","loc":"Gachibowli, Miyapur Road9.22 miles/4.84 kilometers from Hyderabad",
                   "price":"10400.00","dispri":"9400.00","sub":"Air conditioning ,BreakFast Included"}
      ]
    }
    
    ngOnInit() {
      this.checkInDate = this.bookingService.getCheckInDate();
      this.numberOfGuests = this.bookingService.getNumberOfGuests();
      this.checkOutDate = this.bookingService.getCheckOutDate();
    }
  
}
