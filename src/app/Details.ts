export class Details {
    id: number;
    firstname: String;
    lastname: String;
    email: String;
    address: String;
    phonenumber: String;

    constructor(id: number, firstname: String,
        lastname: String,
        email: String,
        address: String,
        phonenumber: String) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.address = address;
        this.phonenumber = phonenumber;
    }
}