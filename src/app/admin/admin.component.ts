import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReviewService } from '../review.service';

interface Review {
  id: number;
  rating: number;
  name: string;
  email: string;
  review: string;
}
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  items: any[] = [];
  updateForm: FormGroup;
  selectedItem: any;
  showUpdateForm: boolean = false;

  feedbackForm: FormGroup;
  feedbacks: Review[] = [];
  submitted = false;

  constructor(private http: HttpClient, private formBuilder: FormBuilder, private feedbackService: ReviewService) {
    this.updateForm = this.formBuilder.group({
      name: [''],
      email: [''],
      gender: [''],
      password: ['']
    });

    this.feedbackForm = this.formBuilder.group({
      rating: ['', Validators.required],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      review: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadItems();
    this.getFeedbacks();
  }

  loadItems() {
    this.http.get<any[]>('http://localhost:8080/showAll').subscribe(data => {
      this.items = data;
    });
  }

  deleteItem(id: number) {
    this.http.delete('http://localhost:8080/delete/${id}').subscribe(() => {
      window.location.reload();
    }, error => {
      console.error('Delete failed:', error);
    });
  }

  updateItem(item: any) {
    this.selectedItem = item;
    this.updateForm.setValue({
      name: item.name,
      email: item.email,
      gender: item.gender,
      password: item.password
    });
    this.showUpdateForm = true;
  }

  submitUpdate() {
    const updatedItem = {
      id: this.selectedItem.id,
      ...this.updateForm.value
    };

    this.http.put('http://localhost:8080/update/${updatedItem.id}', updatedItem).subscribe(() => {
      this.loadItems();
      this.showUpdateForm = false;
    }, error => {
      console.error('Update failed:', error);
    });
  }

  getFeedbacks() {
    this.feedbackService.getAllFeedbacks().subscribe(data => {
      this.feedbacks = data;
    });
  }

}
